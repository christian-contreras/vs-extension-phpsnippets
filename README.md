# phpsnippets README

"phpsnippets"

## Features

Snippets para php



## Extension Settings

Comandos


* `ch_comment`: comentario php
* `ch_tag`: etiquete de php con echo

### Ejemplo 

`ch_comment`
~~~ php
 // ===
 ~~~

![ch_comment](https://gitlab.com/christian-contreras/vs-extension-phpsnippets/raw/master/img/comment.gif)

`ch_tag` 
~~~ php
<?php echo  ; ?>
~~~

![ch_comment](https://gitlab.com/christian-contreras/vs-extension-phpsnippets/raw/master/img/tag_php.gif)


**Enjoy!**
